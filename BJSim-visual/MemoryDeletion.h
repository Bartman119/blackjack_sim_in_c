#pragma once
#ifndef MEMORYDELETION_H_INCLUDED
#define MEMORYDELETION_H_INCLUDED
#include "structures.h"
/**
Function used to remove all used playerHand structure-related memory
@param ppCardsToRemove pointer to pointer to the head of playerHand structure
@return void
*/
void deletePlayerHand(struct playerHand **ppCardsToRemove);

/**
Function used to remove all used Player structure-related memory
@param ppPlayerToRemove pointer to pointer to the head of Player structure
@return void
*/
void deletePlayer(struct Player **ppPlayerToRemove);

/**
Function used to remove all used playerHand structure-related memory
@param ppDeckHeader pointer to pointer to the head of Deck structure
@return void
*/
void deleteDeck(struct Deck **ppDeckHeader);

#endif MEMORYDELETION_H_INCLUDED