#pragma once
#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED
#include "structures.h"
/**
Function reading configuration file's parameters and assigning them to correct variables
@param fileToOpen FILE type pointer containing pointer to config file
@param amountofPlayers variable used to store number of players in game
@param startingMoneyValue parameter used to set starting money for the player
@param amountOfSetsInDeck number of separate sets of Cards used to create one big Deck
@return void
*/
void readFile(FILE * fileToOpen, int *amountOfPlayers, int *startingMoneyValue, int *amountOfSetsInDeck);

/**
Function used to swap places of two values in an array
@param xp pointer to first value
@param yp pointer to second value
@return void
*/
void swap(int *xp, int *yp);

/**
Function used to print all elements of an array
@param arr[] array to be printed
@param size size of an array
@return void
*/
void printArray(int arr[], int size);

/**
Function to implement bubble sort 
@param arr array to be printed
@param n size of an array
@return void
*/
void bubbleSort(int arr[], int n);

/**
Function that prints out scores to file and on console
@param scoreboard an integer array with all high scores
@param ppPlayerHeader pointer to pointer to head of Player structure
@param sizeOfArray integer holding the size of an array
@return void
*/
void printScores(int scoreboard[], struct Player **ppPlayerHeader, int sizeOfArray);





#endif FUNCTIONS_H_INCLUDED
