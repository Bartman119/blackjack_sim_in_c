#include "stdafx.h"
#include <stdio.h>
#include "Structures.h"
#include "FileAndSortFunctions.h"
#define LINE_SIZE 64

void readFile(FILE * fileToOpen, int * amountOfPlayers, int * startingMoneyValue, int * amountOfSetsInDeck)
{
	fileToOpen = fopen("config.txt", "r");
	//checking for correct read of the file
	if (fileToOpen == NULL)
		perror("Error! Couldn't open the file!\n");
	//creating a buffer for reading a line
	char line[LINE_SIZE];
	//creating a string and a double variable for command and variable storage
	char * variableName;
	int variableValue;
	//reading the file until we find EOF
	while (fgets(line, LINE_SIZE, fileToOpen) != NULL)
	{
		//assigning first variable to a command string
		variableName = strtok(line, "=");
		//instantly assigning string after delimiter as a double variable
		variableValue = atoi(strtok(NULL, ";"));
		//assigning values to vertain variables
		if (!strcmp(variableName, "player_amount "))
			*amountOfPlayers = variableValue;
		else if (!strcmp(variableName, "player_starting_cash "))
			*startingMoneyValue = variableValue;
		else if (!strcmp(variableName, "sets_of_cards_in_deck "))
			*amountOfSetsInDeck = variableValue;
		else
			printf("couldn't read data for: %s and %s", variableName, variableValue);
	}
	fclose(fileToOpen);
}


void swap(int *xp, int *yp)
{
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void printArray(int arr[], int size)
{
	int i;
	for (i = 0; i < size; i++)
		printf("%d ", arr[i]);
	//printf("n");
}

void bubbleSort(int arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		// Last i elements are already in place    
		for (j = 0; j < n - i - 1; j++)
			if (arr[j] < arr[j + 1])
				swap(&arr[j], &arr[j + 1]);
	}
}
void printScores(int scoreboard[],struct Player **ppPlayerHeader, int sizeOfArray) 
{
	struct Player *pPlayerHeader = *ppPlayerHeader;
	FILE *fScoreboard = fopen("results.txt", "r");
	//if file doesn't exist, create it and input base values to it
	if (fScoreboard == NULL)
	{
		printf("ERROR! Couldn't open results file! Creating a new one \n");
		fScoreboard = fopen("results.txt", "w");
		for (int i = 1; i <= 10; i++)
		{
			fprintf(fScoreboard, "%d %d\n", i, 0);
		}
		fclose(fScoreboard);
		fScoreboard = fopen("results.txt", "r");
	}
	//read the values from the file
	char line[LINE_SIZE];
	int placement, score, counter = 0;
	while (fgets(line, LINE_SIZE, fScoreboard) != NULL)
	{
		placement = atoi(strtok(line, " "));
		score = atoi(strtok(NULL, " "));
		scoreboard[counter] = score;
		counter++;
	}
	fclose(fScoreboard);
	//if the array doesnt have any values, add new and sort them
	if (scoreboard[0] == 0)
	{
		int counter = 0;
		struct Player *pPlayerPointer = pPlayerHeader;
		while (pPlayerPointer->nextPlayer)
		{
			scoreboard[counter] = pPlayerPointer->currentBalance;
			pPlayerPointer = pPlayerPointer->nextPlayer;
			counter++;
		}
		bubbleSort(scoreboard, sizeOfArray);
		printf("Current leaderboard: \n");
		printArray(scoreboard, sizeOfArray);
	}
	//else compare values and add them in correct places
	else
	{
		int value;
		struct Player *pPlayerPointer = pPlayerHeader;
		while (pPlayerPointer->nextPlayer)
		{
			value = pPlayerPointer->currentBalance;
			for (int i = 0; i < sizeOfArray; i++)
			{
				if (value > scoreboard[i])
				{
					for (int j = 0 + i; j < sizeOfArray; j++)
					{
						int temp = scoreboard[j];
						scoreboard[j] = value;
						value = temp;
					}
				}
			}
			pPlayerPointer = pPlayerPointer->nextPlayer;
		}
		printf("Sorted array: \n");
		printArray(scoreboard, sizeOfArray);
	}
	//now just write to a file 
	FILE *fWriteScores = fopen("results.txt", "w");
	if (fWriteScores == NULL)
		printf("ERROR! Couldn't write into results file!");
	for (int i = 1; i <= sizeOfArray; i++)
	{
		fprintf(fWriteScores, "%d %d\n", i, scoreboard[i - 1]);
	}
	fclose(fWriteScores);

}



