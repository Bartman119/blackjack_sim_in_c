#pragma once
#ifndef STRUCTUREOPERATIONS_H_INCLUDED
#define STRUCTUREOPERATIONS_H_INCLUDED
/**
Function which moves elements of DLL around to simulate Deck shuffling
@param ppDeckHeader pointer to pointer to the header of Deck list
@param numberOfShuffles amount of times to swap places of two cards
@param amountOfDecksInPlay number of sets of Cards a Deck is created from
@return void
*/
void shuffleDeck(struct Deck** ppDeckHeader, int numberOfShuffles, int amountOfDecksInPlay);

/**
Function used to create a new Player structure
@param ppPlayerHeader pointer to pointer to the header of Player list
@param startingCash integer holding amount of points that players start the game with 
@param playerAmount amount of players in game
@return void
*/
void createPlayer(struct Player ** ppPlayerHeader, int startingCash, int playerAmount);

/**
Function creating a Deck structure (a deck of Cards)
@param amountOfDecksInPlay number of sets of Cards a Deck is created from
@param ppDeckHeader pointer to pointer to the header of Deck list
@return void
*/
void createDeck(int amountOfDecksInPlay, struct Deck ** ppDeckHeader);

/**
Function which collects bets from players one by one
@param ppPlayerHeader pointer to pointer to the header of Player list
@param playerAmount amount of players in game
@param isPlaying pointer to a makeshift boolean variable used to check if loop should be still executed
@param isInputCorrect pointer to a makeshift boolean variable used to check if given input is in correct format
@return void
*/
void takeBets(struct Player ** ppPlayerHeader, int playerAmount, int *isPlaying, int*isInputCorrect);

/**
Function used to give out Cards to players
@param ppTempCardHeader pointer to pointer to the header of Deck list that tracks given Cards
@param ppPlayerHeader pointer to pointer to the header of Player list
@param playerAmount amount of players in game
@return void
*/
void giveCards(struct Deck **ppTempCardHeader, struct Player **ppPlayerHeader, int playerAmount);
/**
Function retrieving a name of card and increasing the sum of cards, returns a char pointer (string) with the name of the card
@param yourCard enum with card to read
@param pointer to integer of sum of cards variable
@return string of name of the card 
*/
char * getCard(enum Suit yourCard, int *pSum);

/**
Function retrieving a type of a card, return a char pointer (string) with a name of the type of card
@param yourCard enum with card to read
@return string of the type of the card
*/
char * getType(enum deckType yourCard);

#endif  STRUCTUREOPERATIONS_H_INCLUDED