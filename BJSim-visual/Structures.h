#pragma once
#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED
/**
Enum holding values for different types of card values 
*/
enum Suit { two = 2, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace }; 
/**
Enum holding values for different types of card names
*/
enum deckType { hearts = 1, diamonds, clubs, spades };
/**
Structure holding information about a card
@param oneKindCardSet value of the card
@param kindType value of the type
*/
struct Card
{
	enum Suit oneKindCardSet;
	enum deckType kindType;
};
/**
Structure showing all cards player has in hand during round
@param cardDrawn pointer to sub-structure holding values for the Card
@param nextCard pointer to next Card in player's hand
@param cardValueSum integer holding information about sum of values of the cards
*/
struct playerHand
{
	struct Card * cardDrawn;
	struct playerHand * nextCard;
	int cardValueSum;
};
/**
A singly linked list structure representing players
@param currentBalance integer containing value of points player currently has
@param cashBetThisRound integer containing value of bet money this round
@param cardsInhand pointer to playerHand sub-structure holding all cards a player has during this round
@param nextPlayer pointer to the next Player
*/
struct Player
{
	int currentBalance;
	int cashBetThisRound;
	struct playerHand * cardsInHand;
	struct Player *nextPlayer;
};
/**
A doubly linked list simulating a multiplication of a deck of 52 cards (a DLL of n*52 members)
@param cardInfo pointer to Card sub-structure containing Card information
@param prevCard pointer to previous element in Deck structure
@param nextCard pointer to next element in Deck structure
*/
struct Deck
{
	struct Card *cardInfo;
	struct Deck *prevCard;
	struct Deck *nextCard;
};

#endif // STRUCTURES_H_INCLUDED
