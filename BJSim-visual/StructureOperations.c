#include "stdafx.h"
#include <stdio.h>
#include "Structures.h"
#include "StructureOperations.h"

void createDeck(int amountOfDecksInPlay, struct Deck ** ppDeckHeader)
{
	for (int k = 0; k < amountOfDecksInPlay; k++)
	{
		for (int i = 1; i <= 4; i++)
		{
			for (int j = 2; j <= 14; j++)
			{
				//creating Card structure and filling it with values
				struct Card *oneCard1 = (struct Card*) malloc(sizeof(struct Card));
				oneCard1->oneKindCardSet = j;
				oneCard1->kindType = i;
				//after card(s) is/are created put it in the DLL list
				if (i == 1 && j == 2 && k == 0)
				{
					//create first Deck element and treat it as header
					struct Deck *firstCard = (struct Deck*) malloc(sizeof(struct Deck));
					firstCard->cardInfo = oneCard1;
					firstCard->nextCard = NULL;
					firstCard->prevCard = NULL;
					//assignment of current pointer as header pointer
					*ppDeckHeader = firstCard;
				}
				else
				{
					//create all other Deck elements
					struct Deck *otherCard = (struct Deck*) malloc(sizeof(struct Deck));
					otherCard->cardInfo = oneCard1;
					otherCard->nextCard = NULL;
					otherCard->prevCard = NULL;
					//find last element in the list and assign it as pointer to previous element in current element
					struct Deck *pPointer = *ppDeckHeader;
					while (pPointer->nextCard)
						pPointer = pPointer->nextCard;
					//assign newly created card as next card in deck 
					pPointer->nextCard = otherCard;
					//assign a header and previous card to new card
					otherCard->prevCard = pPointer;
				}
			}
		}
	}
}


void shuffleDeck(struct Deck** ppDeckHeader, int numberOfShuffles, int amountOfDecksInPlay)
{
	//variable used for seeding pseudo-random number generator
	time_t t;
	/* Intializes random number generator */
	srand((unsigned)time(&t));
	for (int i = 0; i<numberOfShuffles; i++) //temporary rng for number of shuffles
	{
		//generate two random numbers that represent index of elements of Deck
		//random numbers from 1 to 52 multiplied by the amount of decks in play
		int amountOfCards = amountOfDecksInPlay * 52;
		int cardNumber1 = rand() % amountOfCards + 1;
		int cardNumber2 = rand() % amountOfCards + 1;
		//creating temporary storage for moved cards 
		struct Card *temp;
		//setting additional pointers
		struct Deck *pPointer1 = *ppDeckHeader;
		struct Deck *pPointer2 = *ppDeckHeader;

		//move to card to shuffle
		for (int j = 0; j < cardNumber1 - 1; j++)
			pPointer1 = pPointer1->nextCard;
		for (int k = 0; k < cardNumber2 - 1; k++)
			pPointer2 = pPointer2->nextCard;

		//store the cards
		temp = pPointer1->cardInfo;
		pPointer1->cardInfo = pPointer2->cardInfo;
		pPointer2->cardInfo = temp;
	}
}

void createPlayer(struct Player ** ppPlayerHeader, int startingCash, int playerAmount)
{
	for (int i = 1; i <= playerAmount + 1; i++) //last object is for Bank! (NPC)
	{
		//create a new player structure
		struct Player *Player1 = (struct Player*) malloc(sizeof(struct Player));
		//set its values with data derived from config file
		Player1->currentBalance = startingCash;
		Player1->cashBetThisRound = 0;
		Player1->cardsInHand = NULL;
		Player1->nextPlayer = NULL;
		//assign as header if it's the first player
		if (i == 1)
			*ppPlayerHeader = Player1;
		//assign to end of the list otherwise
		else
		{
			struct Player *pPointer = *ppPlayerHeader;
			while (pPointer->nextPlayer)
				pPointer = pPointer->nextPlayer;
			pPointer->nextPlayer = Player1;
		}
	}
}

void takeBets(struct Player ** ppPlayerHeader, int playerAmount, int *isPlaying, int *isInputCorrect)
{
	struct Player *pPointer = *ppPlayerHeader;
	for (int i = 1; i <= playerAmount; i++)
	{
		//get the bet from the player
		printf("Player %d, you currently have %d. What's your bet: ", i, pPointer->currentBalance);
		int bet = 0;
		scanf("%d", &bet);
		//if the bet is equal 0, stop the game (ending condition)
		if (bet == 0)
		{
			printf("The game will end now \n");
			*isPlaying = 0;
			break;
		}
		//check if the input is from the correct range 
		else if (bet <= pPointer->currentBalance && bet >= 0)
		{
			pPointer->cashBetThisRound = bet;
			pPointer = pPointer->nextPlayer;
		}
		//input's incorrect, finish the game
		else
		{
			*isInputCorrect = 0;
			break;
		}

	}
}

void giveCards(struct Deck ** ppTempCardHeader, struct Player **ppPlayerHeader, int playerAmount)
{
	//dereferencing pointer to get the real pointer
	struct Deck *dereffedCardHeader = *ppTempCardHeader;
	//giving cards to hands
	for (int i = 0; i < playerAmount + 1; i++)
	{
		struct Player *pPlayerPointer = *ppPlayerHeader;
		for (int j = i; j > 0; j--)
			pPlayerPointer = pPlayerPointer->nextPlayer;
		//create a new hand for player
		struct playerHand *pNewHand = (struct playerHand *) malloc(sizeof(struct playerHand));
		pNewHand->cardDrawn = dereffedCardHeader->cardInfo;
		pNewHand->cardValueSum = 0;
		//move to next card
		dereffedCardHeader = dereffedCardHeader->nextCard;
		//assign next card
		struct playerHand *pNextCard = (struct playerHand *) malloc(sizeof(struct playerHand));
		pNextCard->cardDrawn = dereffedCardHeader->cardInfo;
		pNextCard->cardValueSum = 0;
		pNewHand->nextCard = pNextCard;
		pNewHand->nextCard->nextCard = NULL;
		//assign two pulled cards to a player 
		pPlayerPointer->cardsInHand = pNewHand;
		//move again
		dereffedCardHeader = dereffedCardHeader->nextCard;
	}
	//assign updated list back to ppTempCardHeader
	*ppTempCardHeader = dereffedCardHeader;
}


char * getCard(enum Suit yourCard, int *pSum)
{
	//used to read the name of the card and add it to its sum
	switch (yourCard)
	{
	case 2:
		*pSum += 2;
		return "two\0";
		break;
	case 3:
		*pSum += 3;
		return "three\0";
		break;
	case 4:
		*pSum += 4;
		return "four\0";
		break;
	case 5:
		*pSum += 5;
		return "five\0";
		break;
	case 6:
		*pSum += 6;
		return "six\0";
		break;
	case 7:
		*pSum += 7;
		return "seven\0";
		break;
	case 8:
		*pSum += 8;
		return "eight\0";
		break;
	case 9:
		*pSum += 9;
		return "nine\0";
		break;
	case 10:
		*pSum += 10;
		return "ten\0";
		break;
	case 11:
		*pSum += 10;
		return "jack\0";
		break;
	case 12:
		*pSum += 10;
		return "queen\0";
		break;
	case 13:
		*pSum += 10;
		return "king\0";
		break;
	case 14:
		*pSum += 11;
		return "ace\0";
		break;
	}
}

char * getType(enum deckType yourCard)
{
	//used to get the type of the card
	switch (yourCard)
	{
	case 1:
		return "hearts\0";
		break;
	case 2:
		return "diamonds\0";
		break;
	case 3:
		return "clubs\0";
		break;
	case 4:
		return "spades\0";
		break;
	}
}