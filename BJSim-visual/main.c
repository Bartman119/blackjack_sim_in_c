// BJSim-visual.cpp: definiuje punkt wejścia dla aplikacji konsolowej.
//
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FileAndSortFunctions.h"
#include "Structures.h"
#include "MemoryDeletion.h"
#include "StructureOperations.h"

#define LINE_SIZE 64
#define ARR_SIZE 10
int main()
{
	//creating a pointer 
	FILE *file = NULL;
	//global variables read from program as settings
	int playerAmount, startingCash, setsInDeck;
	int *pPlayerAmount = &playerAmount;
	int *pStartingCash = &startingCash;
	int *pSetsInDeck = &setsInDeck;
	readFile(file,pPlayerAmount,pStartingCash,pSetsInDeck);

	//Create a deck of Cards
	//global pointer for header
	struct Deck *pCardHeader = NULL;
	//pointer to pointer of Deck header, used to pass pointer by reference
	struct Deck **ppCardHeader = &pCardHeader;
	createDeck(setsInDeck, ppCardHeader);
	//after creating a deck we have to shuffle it a random amount of times
	shuffleDeck(ppCardHeader, 200, setsInDeck);

	printf("Welcome to Blackjack Simulator!!!\n");

	//creating header for whole player SLL
	struct Player *pPlayerHeader = NULL;
	//creating pointer to pointer for player SLL
	struct Player **ppPlayerHeader = &pPlayerHeader;
	//create Player structure
	createPlayer(ppPlayerHeader, startingCash, playerAmount);

	//pointer used to remember which cards were already given
	//struct Deck *pTempCardHeader = (struct Deck*) malloc(sizeof(struct Deck));
	struct Deck *pTempCardHeader = pCardHeader;

	//pTempCardHeader = pCardHeader;
	struct Deck **ppTempCardHeader = &pTempCardHeader;

	//variables used to check whether players want to stop playing or inputted wrong characters
	int keepPlaying = 1, shutdown = 1;
	int *pKeepPlaying = &keepPlaying,*pShutdown = &shutdown;
	//a single round of Blackjack Simulator
	while (keepPlaying) 
	{
		printf("---------------------------- ROUND %d -------------------------\n", keepPlaying);
		//collecting bets and assigning them to objects in Player list
		printf("Place your bets, if you'd like to stop playing: press 0\n");
		takeBets(ppPlayerHeader, playerAmount, pKeepPlaying,pShutdown);
		//check if players decided to stop playing
		if (keepPlaying == 0)
			continue;
		//check if input is incorrect and stop the program
		if (shutdown == 0) 
		{
			printf("Incorrect input! Shutting the game down!");
			keepPlaying = 0;
			continue;
		}
		//giving cards to hands
		giveCards(ppTempCardHeader, ppPlayerHeader, playerAmount);
		
		//tell players what bot has as a second card (bot -> last player in the list)
		struct Player *pPlayerPointer = pPlayerHeader;
		//move to last player
		while (pPlayerPointer->nextPlayer)
			pPlayerPointer = pPlayerPointer->nextPlayer;
		//variables used to store values, names of cards and sum of cards in hand
		char * value = NULL, * type = NULL, * value2 = NULL, * type2 = NULL;
		int sum = 0 , decision = 0;
		int * pSum = &sum;
		
		//get value and name of the first card
		value = getCard(pPlayerPointer->cardsInHand->cardDrawn->oneKindCardSet, pSum);
		type = getType(pPlayerPointer->cardsInHand->cardDrawn->kindType);
		
		//get value and name of the second card
		value2 = getCard(pPlayerPointer->cardsInHand->nextCard->cardDrawn->oneKindCardSet, pSum);
		type2 = getType(pPlayerPointer->cardsInHand->nextCard->cardDrawn->kindType);
		
		//assign sum to player
		pPlayerPointer->cardsInHand->cardValueSum = sum;
		struct  playerHand * pGiveSum = pPlayerPointer->cardsInHand;
		while (pGiveSum->nextCard) // go through each card and assign its sum
		{
			pGiveSum = pGiveSum->nextCard;
			pGiveSum->cardValueSum = sum;
		}
		printf("You can see that bank has a %s of %s and one card face-down\n", value,type);
		//now get values of drawn cards for each player
		for (int i = 1; i <= playerAmount; i++)
		{
			sum = 0;
			pPlayerPointer = pPlayerHeader;
			for (int j = i; j > 1; j--)
				pPlayerPointer = pPlayerPointer->nextPlayer;
			value = getCard(pPlayerPointer->cardsInHand->cardDrawn->oneKindCardSet, pSum);
			type = getType(pPlayerPointer->cardsInHand->cardDrawn->kindType);
			
			value2 = getCard(pPlayerPointer->cardsInHand->nextCard->cardDrawn->oneKindCardSet, pSum);
			type2 = getType(pPlayerPointer->cardsInHand->nextCard->cardDrawn->kindType);
			
			pPlayerPointer->cardsInHand->cardValueSum = sum;
			pGiveSum = pPlayerPointer->cardsInHand;
			while (pGiveSum->nextCard) // go through each card and assign its sum
			{
				pGiveSum = pGiveSum->nextCard;
				pGiveSum->cardValueSum = sum;
			}
			printf("Player %d, you have %s of %s and %s of %s. Your sum totals to: %d\n", i, value, type, value2, type2, sum);
			printf("What is your move? 1.Hit or 2.Fold");
			scanf("%d", &decision);
			//a loop for making decisions as a player
			int keepGoing = 1;
			while (keepGoing)
			{
				switch (decision)
				{
				//if the player hits
				case 1:
					//create pointer to cards player holds in his hand and pointer for new card
					struct playerHand * pPlayerCards = pPlayerPointer->cardsInHand;
					struct playerHand * pNewCard = (struct playerHand *) malloc(sizeof(struct playerHand));
					
					//draw a new card from the pile
					pNewCard->cardDrawn = pTempCardHeader->cardInfo;
					pNewCard->nextCard = NULL;
					//assign it to his hand
					while (pPlayerCards->nextCard)
						pPlayerCards = pPlayerCards->nextCard;
					pPlayerCards->nextCard = pNewCard;
					pPlayerCards = pPlayerCards->nextCard;
					//get its value and name and recalculate the sum
					value2 = getCard(pPlayerCards->cardDrawn->oneKindCardSet, pSum);
					type2 = getType(pPlayerCards->cardDrawn->kindType);
					pPlayerCards->cardValueSum = sum;
					//move the pile to the next card
					pTempCardHeader = pTempCardHeader->nextCard; 
					if (sum > 21)
					{
						printf("You pulled %s of %s. Now you have %d! You've lost this round!\n",value2,type2,sum);
						pPlayerPointer->currentBalance = pPlayerPointer->currentBalance - pPlayerPointer->cashBetThisRound;
						keepGoing = 0;
					}
					else
					{
						printf("You pulled %s of %s. Now you have %d! What is your next move? 1. Hit 2.Fold \n",value2,type2, sum);
						scanf("%d", &decision);
					}
					break;
				case 2:
					//player folds
					printf("You folded at %d\n", sum);
					keepGoing = 0;
					break;
				default:
					//if input is incorrect, stop the program
					printf("Incorrect input! Shutting the game down!");
					//delete list of players
					deletePlayer(ppPlayerHeader);
					//delete list of all Cards
					deleteDeck(ppCardHeader);
					return 0;
				}
			}
		}
		//now bank makes a move
		sum = 0;
		pPlayerPointer = pPlayerPointer->nextPlayer;
		value = getCard(pPlayerPointer->cardsInHand->cardDrawn->oneKindCardSet,pSum);
		value2 = getCard(pPlayerPointer->cardsInHand->nextCard->cardDrawn->oneKindCardSet,pSum);
		//if bank has cards worth less than 17, pull cards until he has at least 17
		while (sum < 17) 
		{
			struct playerHand * pPlayerCards = pPlayerPointer->cardsInHand;
			struct playerHand * pNewCard = (struct playerHand *) malloc(sizeof(struct playerHand));
			pNewCard->cardDrawn = pTempCardHeader->cardInfo;
			pNewCard->nextCard = NULL;
			while (pPlayerCards->nextCard)
				pPlayerCards = pPlayerCards->nextCard;
			pPlayerCards->nextCard = pNewCard;
			pPlayerCards = pPlayerCards->nextCard;
			value2 = getCard(pPlayerCards->cardDrawn->oneKindCardSet, pSum);
			pPlayerCards->cardValueSum = sum;
			pTempCardHeader = pTempCardHeader->nextCard;
		}
		//compare values of all players and the bank
		for (int i = 1; i <= playerAmount; i++)
		{
			//get bank's sum
			struct playerHand * pPlayerCards = pPlayerPointer->cardsInHand;
			while (pPlayerCards->nextCard)
				pPlayerCards = pPlayerCards->nextCard;
			//if bank is busted, give all not busted players cash
			if (pPlayerCards->cardValueSum > 21) 
			{
				printf("Bank has %d! All remaining players won!\n", sum);
				struct Player *pPlayerPointer3 = pPlayerHeader;
				while (pPlayerPointer3->nextPlayer) 
				{
					struct playerHand *pPlayerCard = pPlayerPointer3->cardsInHand;
					while (pPlayerCard->nextCard)
						pPlayerCard = pPlayerCard->nextCard;
					if(pPlayerCard->cardValueSum <= 21)
						pPlayerPointer3->currentBalance += pPlayerPointer3->cashBetThisRound;
					pPlayerPointer3 = pPlayerPointer3->nextPlayer;
				}
				break;
			}
			//else compare values of players to bank
			else 
			{
				struct Player *pPlayerPointer2 = pPlayerHeader;
				for (int j = i; j > 1; j--)
					pPlayerPointer2 = pPlayerPointer2->nextPlayer;
				struct playerHand *pPlayerCards2 = pPlayerPointer2->cardsInHand;
				while (pPlayerCards2->nextCard)
					pPlayerCards2 = pPlayerCards2->nextCard;
				if (pPlayerCards2->cardValueSum < pPlayerCards->cardValueSum)
				{
					printf("Player %d, Bank has %d. You've lost!\n", i, pPlayerCards->cardValueSum);
					pPlayerPointer2->currentBalance -= pPlayerPointer2->cashBetThisRound;
				}
				else if (pPlayerCards2->cardValueSum <= 21 && pPlayerCards2->cardValueSum != pPlayerCards->cardValueSum)
				{
					printf("Player %d, Bank has %d. You beat the dealer this round! Congratulations!\n", i, pPlayerCards->cardValueSum);
					pPlayerPointer2->currentBalance += pPlayerPointer2->cashBetThisRound;
				}
				else if(pPlayerCards2->cardValueSum <= 21)
					printf("Player %d, Bank has %d, that's the same amount as you! You keep your bet this round!\n", i, pPlayerCards->cardValueSum);
			}
			
		}
		keepPlaying++;
		//after the round nullify (delete) the cards players held in that round
		struct Player * pDeletePlayerCards = pPlayerHeader;

		for(int i = 0; i < playerAmount + 1; i++)
		{
			struct playerHand* pDeleteCards = pDeletePlayerCards->cardsInHand;
			struct playerHand** ppDeleteCards = &pDeleteCards;
			deletePlayerHand(ppDeleteCards);
			pDeletePlayerCards = pDeletePlayerCards->nextPlayer;
		}
	}

	printf("Printing high scores to results.txt file! \n");
	//create a file with top 10 scores of all time 
	int scoreboard[ARR_SIZE] = { 0,0,0,0,0,0,0,0,0,0 };
	//print high scores to file and on console
	printScores(scoreboard, ppPlayerHeader, ARR_SIZE);

	//delete list of players
	deletePlayer(ppPlayerHeader);
	//delete list of all Cards
	deleteDeck(ppCardHeader);

	_CrtDumpMemoryLeaks();
	return 0;
}

