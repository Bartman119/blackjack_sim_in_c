#include "stdafx.h"
#include <stdio.h>
#include "Structures.h"
#include "MemoryDeletion.h"


void deletePlayerHand(struct playerHand **ppCardsToRemove)
{
	//declare two pointers
	struct playerHand * pRemoveFromList = *ppCardsToRemove;
	struct playerHand * pRemoveFromListNext;
	//move through the list and delete elements in structure one by one
	while (pRemoveFromList)
	{
		pRemoveFromListNext = pRemoveFromList->nextCard;
		free(pRemoveFromList);
		pRemoveFromList = pRemoveFromListNext;
	}
	//remove the whole list pointer
	*ppCardsToRemove = NULL;
}


void deletePlayer(struct Player **ppPlayerToRemove)
{
	//declare two pointers
	struct Player *pRemovePlayer = *ppPlayerToRemove;
	struct Player *pRemovePlayerNext;
	//move through the list and delete elements in structure one by one
	while (pRemovePlayer)
	{
		pRemovePlayerNext = pRemovePlayer->nextPlayer;
		free(pRemovePlayer);
		pRemovePlayer = pRemovePlayerNext;
	}
	*ppPlayerToRemove = NULL;
}

void deleteDeck(struct Deck **ppDeckHeader)
{
	struct Deck * pRemoveDeck = *ppDeckHeader;
	struct Deck * pRemoveDeckNext;
	while (pRemoveDeck->nextCard)
	{
		pRemoveDeckNext = pRemoveDeck->nextCard;
		free(pRemoveDeck->cardInfo);
		free(pRemoveDeck);
		pRemoveDeck = pRemoveDeckNext;
	}
	free(pRemoveDeck->cardInfo);
	free(pRemoveDeck);
	*ppDeckHeader = NULL;
}