The following folder contains files for BlackJack Simulator program written in c language.
Program simulates a game of casino blackjack for a fixed amount of players.
It features bank NPC that players play against and regular bidding and card draw algorithm.
Program uses custom structures and memory operations to store and operate on cards. 